# websocket

- 双向
- 性能高
- socket.io


服务端：
sock.on('connection')
sock.on('disconnect')

客户端：
sock.on('connect')
sock.on('disconnect')

WebSocket是前台的东西，是HTML5带的一种东西
1.只有前台有WebSocket这个东西
2.后台没有，后台有Socket

用WebSocket：
1.socket.io
2.原生WebSocket
  i.net模块
  ii.流程
    a.握手
      C:version:13、sec-websocket-key:xxxxx、sha1(key+mask)=>base64
      S:101 Switching Protocols、sec-websocket-accept: base64
      C <-> S

      Client：
      onopen
      onmessage
      onclose

      Server:
      net.createServer(sock=>{});
      sock.once('data', 握手);
      sock.on('data', 数据请求);
      sock.on('end');

    b.数据帧解析



