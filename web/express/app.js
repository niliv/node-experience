var express = require('express')
var path = require('path')

var app = express();

//设置模板引擎
app.set('view engine', 'ejs')

//引入外部样式表
app.use('/style', express.static('style'))

//路由
app.get('/',function(req,res){
    console.log(req.url)
    var data;
    res.render('index',data);
})
app.get('/contact',function(req,res){
    console.log(req.url)
    var data;
    res.render('contact',data);
})
//路由参数 ejs模板
app.get('/profile/:content',function(req,res){
    console.log(req.url)
    var data = {age:29,name: 'henry'};
    var array = [{age:29,name: 'henry'},{age:30,name: 'bucky'}]
    var array1 = [{age:29,name: ['yk1','yk2']},{age:30,name: ['kk1','kk2']}]
    res.render('profile',{name: req.params.content,data: data,array:array,array1:array1});
})

app.listen(9999);