const fs = require('fs')
const path =require('path')
const http = require('http')

var server = http.createServer(function(req,res){
    if(req.url!=="/favicon.ico"){
        if(req.url === '/home' || req.url === '/'){
            res.writeHead(200,{"Content-type": "text/html;charset=utf-8"})
            fs.createReadStream(path.join(__dirname,'/index.html')).pipe(res);
        }else if(req.url === '/contact'){
            res.writeHead(200,{"Content-type": "text/html;charset=utf-8"})
            fs.createReadStream(path.join(__dirname,'/contact.html')).pipe(res);
        }else if(req.url === '/api'){
            res.writeHead(200,{"Content-type": "application/json;charset=utf-8"})
            let data = [{name: "yuankun", age: 35}];
            res.end(JSON.stringify(data))
        }
    }
})
server.listen(7777,'127.0.0.1');
console.log('server is running...6666')