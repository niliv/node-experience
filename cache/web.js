const http = require('http');
const fs = require('fs');
const zlib = require('zlib');

http.createServer((req, res) => {
  console.log(req.url)
  if (req.url === '/') {
    let rs = fs.createReadStream('index.html');
    //rs.pipe(res);
    res.setHeader('content-encoding', 'gzip');
    let gz = zlib.createGzip();
    rs.pipe(gz).pipe(res);
    rs.on('error', err => {
      res.writeHeader(404);
      res.write('Not Found');

      res.end();
    });
  }
  if (req.url === '/script.js') {
    if (req.headers['if-none-match'] === '777') {
      res.writeHeader(304, {
        'Content-Type': 'text/javascript',
        'Cache-Control': 'max-age=20000,no-cache',
        'Etag': '777'
      });
      res.end('console.log("script load have cache!")');
    } else {
      res.writeHeader(200, {
        'Content-Type': 'text/javascript',
        'Cache-Control': 'max-age=20000,no-cache',
        'Etag': '777'
      });
      res.end('console.log("script load have alter!")');
    }
  }
  //要更新缓存可以在js后面加东西 vue.122141214214124211.js
  if (req.url === '/vue.js') {
    let rs = fs.createReadStream('vue.js');
    //rs.pipe(res);
    res.setHeader('Cache-Control', 'max-age=30');
    let gz = zlib.createGzip();
    rs.pipe(gz).pipe(res);
  }

}).listen(9998)