const http=require('http');
const mysql=require('mysql');
const fs=require('fs');
const url=require('url');
const zlib=require('zlib');
const crypto=require('crypto');

//加密
const _key='tsd1fghjnm0~!@2#$%qwe3r^&*4()_+`yu9io5pa-=[]6{}|;:<>7,klzx8cvb.?MMBB@!520';
function md5(str){
    let obj = crypto.createHash('md5');
    obj.update(str);
    return obj.digest('hex');
}
function myMd5(str){
    return md5(md5(str)+_key);
}
//建立数据库连接
let db=mysql.createPool({
    host: '118.24.175.34',
    user: 'root',
    password: 'p@ssw0rd',
    port: 3306,
    database: '20190216'
})
//httpserver
let server = http.createServer((req,res)=>{

    res.writeHead(200, {
        'Access-Control-Allow-Origin': 'http://127.0.0.1:5500',
        'Access-Control-Allow-Headers': 'X-Test-Cors',
        'Access-Control-Allow-Methods': 'POST, PUT, DELETE',
        'Access-Control-Max-Age': '1000'
    })

    let {pathname,query} = url.parse(req.url,true);
    let {user,pass} = query;
    switch(pathname){
        case '/reg':
            if(!user){
                res.write('{"errCode": 1, "msg": "username can\'t be null"}');
                res.end();
            }else if(!pass){
                res.write('{"errCode": 1, "msg": "password can\'t be null"}');
                res.end();
            }else if(!/^\w{4,16}$/.test(user)){
                res.write('{"errCode": 1, "msg": "username is invaild"}');
                res.end();
            }else if(/['|"]/.test(pass)){
                res.write('{"errCode": 1, "msg": "password is invaild"}');
                res.end();
            }else{
                db.query(`SELECT * FROM user WHERE username='${user}'`,(err,data)=>{
                    if(err){
                        res.write('{"errCode": 1, "msg":  "database error"}');
                        res.end();
                    }else if(data.length>0){
                        res.write('{"errCode": 1, "msg":  "this username exsits"}');
                        res.end();
                    }else{
                        db.query(`INSERT INTO user (username,password) VALUES('${user}','${myMd5(pass)}')`,(err,data)=>{
                            if(err){
                                res.write('{"errCode": 1, "msg": "database error"}');
                                res.end();
                            }else{
                                res.write('{"errCode": 0, "msg": "success"}');
                                res.end();
                            }
                        })
                    }
                })
            }
            break;
        case '/login':
            if(!user){
                res.write('{"err": 1, "msg": "username can\'t be null"}');
                res.end();
            }else if(!pass){
                res.write('{"err": 1, "msg": "password can\'t be null"}');
                res.end();
            }else if(!/^\w{4,16}$/.test(user)){
                res.write('{"err": 1, "msg": "username is invaild"}');
                res.end();
            }else if(/['|"]/.test(pass)){
                res.write('{"err": 1, "msg": "password is invaild"}');
                res.end();
            }else{
                db.query(`SELECT * FROM user WHERE username='${user}'`, (err, data)=>{
                if(err){
                    res.write('{"err": 1, "msg": "database error"}');
                    res.end();
                }else if(data.length==0){
                    res.write('{"err": 1, "msg": "no this user"}');
                    res.end();
                }else if(data[0].password!=myMd5(pass)){
                    res.write('{"err": 1, "msg": "username or password is incorrect"}');
                    res.end();
                }else{
                    res.write('{"err": 0, "msg": "success"}');
                    res.end();
                }
                });
            }
            break;
        default:
            //缓存
            //处理静态文件
            let rs = fs.createReadStream(`www${pathname}`);
            let gz = zlib.createGzip();
            res.setHeader('content-encoding', 'gzip');
            rs.pipe(gz).pipe(res);
            rs.on('error', err=>{
                res.writeHeader(404);
                res.write('Not Found');
                res.end();
            });
    }
});
server.listen(9999);

