const express = require('express')
const mysql = require('mysql')
const app = express()

//创建连接
var connection = mysql.createConnection({
  host     : '118.24.175.34',
  user     : 'root',
  password : 'p@ssw0rd',
  database : 'mysqltest'
});

connection.connect((err)=>{
    if(err) throw err;
    console.log('mysql is connecting')
});

app.get("/querydb/:id",(req,res)=>{
    let sql = `SELECT * FROM user WHERE openid="${req.params.id}"`;
    connection.query(sql,(err,result)=>{
        if(err) throw err;
        console.log(result)
        res.json(result)
    })
})
app.get("/adddb",(req,res)=>{
    let user = {openid: '456456',name: 'mm',ico:'ico.png',desc:'333'}
    let sql = 'INSERT INTO user SET ?';
    connection.query(sql,user,(err,result)=>{
        if(err) throw err;
        console.log(result)
        res.send('add success')
    })
})
app.get("/updatedb/:id",(req,res)=>{
    let desc = 'update test';
    let sql = `UPDATE user SET description="${desc}" WHERE openid="${req.params.id}"`;
    console.log(sql)
    connection.query(sql,(err,result)=>{
        if(err) throw err;
        console.log(result)
        res.json(result)
    })
})
app.get("/deletedb/:id",(req,res)=>{
    let sql = `DELETE FROM user WHERE openid="${req.params.id}"`;
    console.log(sql)
    connection.query(sql,(err,result)=>{
        if(err) throw err;
        console.log(result)
        res.json(result)
    })
})




app.listen("3000",()=>{
    console.log('Server started on 3000')
})