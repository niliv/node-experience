# my-node

**vscode 调试**
`npm init -y`
修改 `package.json`中的`main`，确定启动文件
打断点

- [常用模块](api.md)
- [server](server.md)
- [cache](cache.md)
- [web](web.md)
- [express](web/express/app.js)
- [com](com.md)
- [sql](sql.md)
- [websocket](websocket.md)

## experience

### server

[静态服务器 数据服务器 文件上传服务器](server/server.js)

### cache

[缓存操作 1](cache/cache.js)
[缓存操作 2](cache/web.js)

### sql

[mysql](sql/mysql/app.js)
[登录注册](sql/login/login.js)

### websocket

[实现 websocket](websocket/raw_server2.js)
[聊天室/socket.io](websocket/chatroom/server.js)

### web

[express](web/express/app.js)

[express](express.md)

[koa](koa.md)

[pm2](pm2.md)

### 项目

[todoApp](demo/todoApp/app.js)
[courseApp](demo/todoApp/app.js)
[nodeApp](demo/todoApp/server.js)

### kkb 课程代码

[kkb](code_back)
