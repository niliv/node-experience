﻿# 缓存

[web缓存详解](http://www.alloyteam.com/2012/03/web-cache-1-web-cache-overview/)

**第一重要、缓存策略**
cache-control
expires

**第二重要、缓存实现过程**
1. 第一次S->C："Last-Modified: Sat, 02 Dec 2017 04:03:14 GMT"
2. 第二次C->S："If-Modified-Since: Sat, 02 Dec 2017 04:03:14 GMT"
3. 第二次S->C：200 || 304

**cache-control**
nocache不缓存
**浏览器在max-age时间内都会缓存文件，下次发布的时候在静态资源名后跟上has码就能更新**

```javascript
if(req.url==='/script.js'){
  res.setHeader('Content-Type','text/javascript');
  res.setHeader('Cache-Control','max-age=200');
  res.end('console.log("script load have cache!")');
}
```

**有缓存的静态服务器**
浏览器Disable cache大于一切
Cache-Control大于服务器验证

Etag if-none-match
```javascript
if(req.headers['if-none-match'] === '777'){
  res.writeHeader(304,{
    'Content-Type':'text/javascript',
    'Cache-Control':'max-age=20000,no-cache',
    'Etag':'777'
  });
  res.end('console.log("script load have cache!")');
}
```
Last-Modified If-Modified-Since 
```javascript
const http=require('http');
const fs=require('fs');
const url=require('url');

http.createServer((req, res)=>{
  let {pathname}=url.parse(req.url);

  //获取文件日期
  fs.stat(`www${pathname}`, (err, stat)=>{
    if(err){
      res.writeHeader(404);
      res.write('Not Found');
      res.end();
    }else{
      if(req.headers['if-modified-since']){
        let oDate=new Date(req.headers['if-modified-since']);
        let time_client=Math.floor(oDate.getTime()/1000);

        let time_server=Math.floor(stat.mtime.getTime()/1000);

        if(time_server>time_client){      //服务器的文件时间>客户端手里的版本
          sendFileToClient();
        }else{
          res.writeHeader(304);
          res.write('Not Modified');
          res.end();
        }
      }else{
        sendFileToClient();
      }

      function sendFileToClient(){
        //发送
        let rs=fs.createReadStream(`www${pathname}`);

        res.setHeader('Last-Modified', stat.mtime.toGMTString());

        //输出
        rs.pipe(res);

        rs.on('error', err=>{
          res.writeHeader(404);
          res.write('Not Found');
          res.end();
        });
      }
    }
  });
}).listen(9999);
```