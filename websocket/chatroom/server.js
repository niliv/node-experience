const http=require('http');
const io=require('socket.io');

let httpServer = http.createServer((req,res)=>{});
httpServer.listen(9999);

let wsServer = io.listen(httpServer);

let aSock=[];

wsServer.on('connection', sock=>{
    aSock.push(sock);

    sock.on('msg', chat=>{
        aSock.forEach(s=>{
            s.emit('msg',chat);
        })
    });

    sock.on('disconnect', ()=>{
        let n=aSock.indexOf(sock);
        if(n!=-1){
            aSock.splice(n, 1);
        }
    });

})

setInterval(function (){
    console.log(aSock.length);
  }, 500);