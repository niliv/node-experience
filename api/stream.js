const fs = require('fs')
const http = require('http')
/**流 类似java io流 */

//文件流,
/* 
var inputStream = fs.createReadStream(__dirname+'/static/test.txt','utf8')
var outputStream = fs.createWriteStream(__dirname+'wtest1.txt')
inputStream.pipe(outputStream); */
/* inputStream.on('data',function(chunk){
    console.log('分割线-------------------')
    outputStream.write(chunk)
}) */

/**网络流 */
//返回文本
/* var server = http.createServer(function(req,res){
    res.writeHead(200,
        {
            "Content-type": "text/plain;charset=utf-8"
        }
    )
    var inputStream = fs.createReadStream(__dirname+'/static/test.txt','utf8')
    inputStream.pipe(res);
})
server.listen(7777,'127.0.0.1');
console.log('server is running...') */

//返回html
/* var server = http.createServer(function(req,res){
    res.writeHead(200,
        {
            "Content-type": "text/html;charset=utf-8"
        }
    )
    var inputStream = fs.createReadStream(__dirname+'/static/index.html','utf8')
    inputStream.pipe(res);
})
server.listen(7777,'127.0.0.1');
console.log('server is running...') */

//返回json
var server = http.createServer(function(req,res){
    if(req.url!=="/favicon.ico"){
        res.writeHead(200,
            {
                "Content-type": "application/json;charset=utf-8"
            }
        )
        var inputStream = fs.createReadStream(__dirname+'/static/resume.json','utf8')
        inputStream.pipe(res);
    }
})
server.listen(7777,'127.0.0.1');
console.log('server is running...')