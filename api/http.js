const http = require('http')

var server = http.createServer(function(req,res){
    console.log(req.url)
    res.writeHead(200,
        {
            "Content-type": "text/plain"
        }
    )
    res.end("server is working");
})

server.listen(8888,'127.0.0.1');
console.log('server is running...')