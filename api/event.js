/*事件*/
let events = require('events');

let myEmitter = new events.EventEmitter();

myEmitter.on('someEvent',function (msg) {
    setImmediate(()=>{ // 实现异步
        console.log(msg);
    })
});
myEmitter.emit('someEvent','实现事件，传给回调');

console.log(1);