# 服务器

- get 数据
  url 里面
  小-32K

- post 数据
  作为 body
  大-1G

- setHeader() 可以写多个头
- writeHeader() 只能写一个直接返回
- write()

安全性：99.9%的漏洞都是懒

1. 一切来自前台的数据都不可信
2. 前后台都得进行数据校验
   前台校验：用户体验
   后台校验：安全性

每个机器一共有 65535（2 的 16 次方减 1）个端口（这是协议规定的）
0 到 1023（2 的 10 次方减 1）号端口是留给系统使用的，你只有拥有了管理员权限后，才能使用这 1024 个端口
[端口列表](https://zh.wikipedia.org/wiki/TCP/UDP%E7%AB%AF%E5%8F%A3%E5%88%97%E8%A1%A8#0.E5.88.B01023.E5.8F.B7.E7.AB.AF.E5.8F.A3)

数据库：

1. 关系型数据库——MySQL、Oracle 最常见、最常用
   数据之间是有关系的
2. 文件型数据库——sqlite
   简单、小
3. 文档型数据库——MongoDB
   直接存储异构数据——方便
4. NoSQL 没有复杂的关系、对性能有极高的要求
   redis、memcached、hypertable、bigtable

## 处理请求

```javascript
/**处理get请求 */
const http = require("http");
const querystring = require("querystring");
const url = require("url");

http
  .createServer((req, res) => {
    console.log(req.method);
    const url = req.url;
    req.query = querystring.parse(url.split("?")[1]);
    console.log(req.query);
    res.end(JSON.stringify(req.query));
  })
  .listen(8000);

/**处理post请求 */
const http = require("http");
const querystring = require("querystring");
const url = require("url");

http
  .createServer((req, res) => {
    console.log(req.method);
    console.log(req.headers["content-type"]);

    let postData = "";
    req.on("data", chunk => {
      postData += chunk;
    });
    req.on("end", () => {
      console.log(postData);
      let post = querystring.parse(postData);
      console.log(post);
      res.end("hello");
    });
  })
  .listen(8000);
```

## 静态资源服务器

```javascript
const http = require("http");
const fs = require("fs");
let server = http.createServer((req, res) => {
  fs.readFile(`www${req.url}`, (err, data) => {
    console.log(`www${req.url}`);
    if (err) {
      //返回状态
      res.writeHeader(404);
      //返回数据
      res.write("not found");
    } else {
      //write要么字符串要么二进制
      res.write(data);
    }
    res.end();
  });
});
server.listen(9999);
```

## 数据交互服务器

```javascript
const http = require("http");
const url = require("url");
const querystring = require("querystring");
const fs = require("fs");

let users = {
  //  'blue': '123456',
  //  'zhangsan': '654321'
};

let server = http.createServer((req, res) => {
  res.writeHead(200, {
    "Access-Control-Allow-Origin": "http://127.0.0.1:5500",
    "Access-Control-Allow-Headers": "X-Test-Cors",
    "Access-Control-Allow-Methods": "POST, PUT, DELETE",
    "Access-Control-Max-Age": "1000"
  });
  //GET
  let { pathname, query } = url.parse(req.url, true);

  //POST
  let str = "";
  req.on("data", data => {
    str += data;
  });
  req.on("end", () => {
    let post = querystring.parse(str);

    let { user, pass } = query;

    //写东西
    switch (pathname) {
      case "/reg": //注册
        if (!user) {
          res.write('{"err": 1, "msg": "user is required"}');
        } else if (!pass) {
          res.write('{"err": 1, "msg": "pass is required"}');
        } else if (!/^\w{8,32}$/.test(user)) {
          res.write('{"err": 1, "msg": "invaild username"}');
        } else if (/^['|"]$/.test(pass)) {
          res.write('{"err": 1, "msg": "invaild password"}');
        } else if (users[user]) {
          res.write('{"err": 1, "msg": "username already exsits"}');
        } else {
          users[user] = pass;
          res.write('{"err": 0, "msg": "success"}');
        }
        res.end();
        break;
      case "/login": //登陆
        if (!user) {
          res.write('{"err": 1, "msg": "user is required"}');
        } else if (!pass) {
          res.write('{"err": 1, "msg": "pass is required"}');
        } else if (!/^\w{8,32}$/.test(user)) {
          res.write('{"err": 1, "msg": "invaild username"}');
        } else if (/^['|"]$/.test(pass)) {
          res.write('{"err": 1, "msg": "invaild password"}');
        } else if (!users[user]) {
          res.write('{"err": 1, "msg": "no this user"}');
        } else if (users[user] != pass) {
          res.write('{"err": 1, "msg": "username or password is incorrect"}');
        } else {
          res.write('{"err": 0, "msg": "login success"}');
        }
        res.end();
        break;
      default:
        //其他：文件
        fs.readFile(`www${pathname}`, (err, data) => {
          if (err) {
            res.writeHeader(404);
            res.write("Not Found");
          } else {
            res.write(data);
          }

          res.end();
        });
    }
  });
});
server.listen(9999);
```

file 上传，也是 post

表单的三种 POST：

1. text/plain  
   用的很少，纯文字
2. application/x-www-form-urlencoded  
   默认 url 编码方式，xxx=xxx&xxx=xx...
3. multipart/form-data  
   上传文件内容

## 文件上传服务器

Buffer 操作

- 查找：indexOf
- 截取：slice
- 切分：

```javascript
//common
Buffer.prototype.split =
  Buffer.prototype.split ||
  function(b) {
    let arr = [];

    let cur = 0;
    let n = 0;
    while ((n = this.indexOf(b, cur)) != -1) {
      arr.push(this.slice(cur, n));
      cur = n + b.length;
    }

    arr.push(this.slice(cur));

    return arr;
  };
//server
const http = require("http");
const common = require("./libs/common");
const fs = require("fs");
const uuid = require("uuid/v4");

let server = http.createServer((req, res) => {
  let arr = [];

  req.on("data", data => {
    arr.push(data);
  });
  req.on("end", () => {
    let data = Buffer.concat(arr);

    //data
    //解析二进制文件上传数据
    let post = {};
    let files = {};
    if (req.headers["content-type"]) {
      let str = req.headers["content-type"].split("; ")[1];
      if (str) {
        let boundary = "--" + str.split("=")[1];

        //1.用"分隔符切分整个数据"
        let arr = data.split(boundary);

        //2.丢弃头尾两个数据
        arr.shift();
        arr.pop();

        //3.丢弃掉每个数据头尾的"\r\n"
        arr = arr.map(buffer => buffer.slice(2, buffer.length - 2));

        //4.每个数据在第一个"\r\n\r\n"处切成两半
        arr.forEach(buffer => {
          let n = buffer.indexOf("\r\n\r\n");

          let disposition = buffer.slice(0, n);
          let content = buffer.slice(n + 4);

          disposition = disposition.toString();

          if (disposition.indexOf("\r\n") == -1) {
            //普通数据
            //Content-Disposition: form-data; name="user"
            content = content.toString();

            let name = disposition.split("; ")[1].split("=")[1];
            name = name.substring(1, name.length - 1);

            post[name] = content;
          } else {
            //文件数据
            /*Content-Disposition: form-data; name="f1"; filename="a.txt"\r\n
            Content-Type: text/plain*/
            let [line1, line2] = disposition.split("\r\n");
            let [, name, filename] = line1.split("; ");
            let type = line2.split(": ")[1];

            name = name.split("=")[1];
            name = name.substring(1, name.length - 1);

            filename = filename.split("=")[1];
            filename = filename.substring(1, filename.length - 1);

            let path = `upload/${uuid().replace(/\-/g, "")}`;

            fs.writeFile(path, content, err => {
              if (err) {
                console.log("文件写入失败", err);
              } else {
                files[name] = { filename, path, type };
                console.log(files);
              }
            });
          }
        });

        //5.完成
        console.log(post);
      }
    }

    res.end();
  });
});
server.listen(8080);
```

readFile 先把所有数据全读到内存中，然后回调：

1. 极其占用内存
2. 资源利用极其不充分

**流**

- 读取流
- 写入流
- 读写流

静态服务器升级

```javascript
const fs = require("fs");

let rs = fs.createReadStream("1.png");
let ws = fs.createWriteStream("2.png");

rs.pipe(ws);

rs.on("error", err => {
  console.log("读取失败");
});

ws.on("finish", () => {
  console.log("写入完成");
});

const http = require("http");
const fs = require("fs");
const zlib = require("zlib");

let server = http.createServer((req, res) => {
  let rs = fs.createReadStream(`www${req.url}`);

  //rs.pipe(res);

  res.setHeader("content-encoding", "gzip");

  let gz = zlib.createGzip();
  rs.pipe(gz).pipe(res);

  rs.on("error", err => {
    res.writeHeader(404);
    res.write("Not Found");

    res.end();
  });
});
server.listen(9999);
```
