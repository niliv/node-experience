# 常用模块

 - stuff 模块 [demo](stuff.js)
 - assert 断言 [demo](assert.js)
 - crypto 加密 [demo](crypto.js)
 - event 事件 [demo](event.js)
 - file 文件 [demo](file.js)
 - path 路径 [demo](path.js)
 - http http [demo](http.js)
 - stream 流 [demo](stream.js)
 - url url/query [demo](url.js)

