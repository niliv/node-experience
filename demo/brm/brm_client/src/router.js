import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/index"
    },
    {
      path: "/index",
      name: "index",
      component: () => import("@/views/index.vue"),
      children: [
        { path: "", component: () => import("@/views/Home.vue") },
        {
          path: "/home",
          name: "home",
          component: () => import("@/views/Home.vue")
        },
        {
          path: "/infoshow",
          name: "infoshow",
          component: () => import("@/views/InfoShow.vue")
        },
        {
          path: "/foundlist",
          name: "foundlist",
          component: () => import("@/views/FoundList.vue")
        }
      ]
    },
    {
      path: "/register",
      name: "register",
      component: () => import("@/views/Register.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/Login.vue")
    },
    {
      path: "*",
      name: "/404",
      component: () => import("@/views/404.vue")
    }
  ]
});

// 添加路由守卫
router.beforeEach((to, from, next) => {
  const isLogin = localStorage.eleToken ? true : false;
  if (to.path == "/login" || to.path == "/register") {
    next();
  } else {
    isLogin ? next() : next("/login");
  }
});

export default router;
