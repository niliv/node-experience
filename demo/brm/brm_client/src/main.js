import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import Vue from "vue";
import App from "./App.vue";
import axios from "./http";
import router from "./router";
import store from "./store/index";

Vue.config.productionTip = false;
Vue.use(ElementUI);

Vue.prototype.$axios = axios;
console.log(process.env);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
