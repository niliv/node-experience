const state = {
  // 需要维护的状态
  isAutnenticated: false, // 是否认证
  user: {} // 存储用户信息
};

const getters = {
  isAutnenticated: state => state.isAutnenticated,
  user: state => state.user
};

const mutations = {
  setIsAutnenticated(state, isAutnenticated) {
    if (isAutnenticated) state.isAutnenticated = isAutnenticated;
    else state.isAutnenticated = false;
  },
  setUser(state, user) {
    if (user) state.user = user;
    else state.user = {};
  }
};

const actions = {
  setIsAutnenticated: ({ commit }, isAutnenticated) => {
    commit("setIsAutnenticated", isAutnenticated);
  },
  setUser: ({ commit }, user) => {
    commit("setUser", user);
  },
  clearCurrentState: ({ commit }) => {
    commit("setIsAutnenticated", false);
    commit("setUser", null);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
