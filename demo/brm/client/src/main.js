import Blob from "@/excel/Blob.js";
import Export2Excel from "@/excel/Export2Excel.js";
import ElTreeSelect from "el-tree-select";
import ElementUI from "element-ui";
import moment from "moment";
import Vue from "vue";
import "../theme/index.css";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";

Vue.filter("dateformat", function(dataStr, pattern = "YYYY-MM-DD HH:mm:ss") {
  return moment(dataStr).format(pattern);
});

var falseBlob = Blob;
var falseExport2Excel = Export2Excel;
Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(ElTreeSelect);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
