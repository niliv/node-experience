import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";
import role from "./modules/role";
import user from "./modules/user";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    user,
    role
  },
  getters
});

export default store;
