import userApi from "@/api/userApi";
import { Message } from "element-ui";
import jwt_decode from "jwt-decode";
import router from "../../router";
import { isEmpty } from "../../utils/validator";
const user = {
  state: {
    // 需要维护的状态
    isAutnenticated: false, // 是否认证
    user: {} // 存储用户信息
  },

  mutations: {
    setIsAutnenticated(state, isAutnenticated) {
      if (isAutnenticated) state.isAutnenticated = isAutnenticated;
      else state.isAutnenticated = false;
    },
    setUser(state, user) {
      if (user) state.user = user;
      else state.user = {};
    }
  },

  actions: {
    setIsAutnenticated: ({ commit }, isAutnenticated) => {
      commit("setIsAutnenticated", isAutnenticated);
    },
    setUser: ({ commit }, user) => {
      commit("setUser", user);
    },
    clearCurrentState: ({ commit }) => {
      commit("setIsAutnenticated", false);
      commit("setUser", null);
    },
    login: ({ commit }, loginUser, payload = { path: "/index" }) => {
      return userApi.login(loginUser).then(res => {
        console.log(res);
        if (res.data.msg && res.data.msg.errno == 0) {
          const { token } = res.data;
          localStorage.setItem("eleToken", token);

          // // 解析token
          const decode = jwt_decode(token);
          console.log(decode);
          // // 存储数据
          commit("setIsAutnenticated", !isEmpty(decode));
          commit("setUser", decode);

          // 页面跳转
          router.push(payload);
        } else {
          Message.error(res.data.message);
        }
      });
    }
  }
};
export default user;
