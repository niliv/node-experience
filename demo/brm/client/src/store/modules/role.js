import menuApi from "@/api/menuApi";
import roleApi from "@/api/roleApi";
const role = {
  state: {
    role: {}, // 存储用户信息
    menus: []
  },

  mutations: {
    setRole(state, role) {
      sessionStorage.setItem("role", JSON.stringify(role));
      state.role = role;
    },
    setMenus(state, menus) {
      sessionStorage.setItem("menus", JSON.stringify(menus));
      state.menus = menus;
    }
  },

  actions: {
    getRoles: ({ commit }, name) => {
      return roleApi.getRolesByName(name).then(res => {
        console.log(res);
        if (res.data.msg && res.data.msg.errno == 0) {
          commit("setRole", res.data.role);
          menuApi
            .getMenusByIds(res.data.role.menus)
            .then(items => {
              console.log(items);
              commit("setMenus", items.data.menus);
            })
            .catch(err => {
              console.log(err);
              this.$message.error("网络错误");
            });
        } else {
          Message.error(res.data.message);
        }
      });
    },
    setRole: ({ commit }, role) => {
      commit("setRole", role);
    },
    setMenus: ({ commit }, menus) => {
      commit("setMenus", menus);
    }
  }
};
export default role;
