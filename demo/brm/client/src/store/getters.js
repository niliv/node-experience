const getters = {
  isAutnenticated: state => state.user.isAutnenticated,
  user: state => state.user.user,
  role: () => JSON.parse(sessionStorage.getItem("role")),
  menus: () => JSON.parse(sessionStorage.getItem("menus"))
};
export default getters;
