import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/login"
    },
    {
      path: "/index",
      name: "index",
      component: () => import("@/views/index.vue"),
      children: [
        { path: "", component: () => import("@/views/Home.vue") },
        {
          path: "/home",
          name: "home",
          component: () => import("@/views/Home.vue")
        },
        {
          path: "/infoshow",
          name: "infoshow",
          component: () => import("@/views/InfoShow.vue")
        },
        {
          path: "/foundlist",
          name: "foundlist",
          component: () => import("@/views/FoundList.vue")
        },
        {
          path: "/userlist",
          name: "userlist",
          component: () => import("@/views/UserList.vue")
        },
        {
          path: "/layout",
          name: "layout",
          component: () => import("@/views/layout.vue")
        }
      ]
    },
    {
      path: "/register",
      name: "register",
      component: () => import("@/views/Register.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/Login.vue")
    },
    {
      path: "*",
      name: "/404",
      component: () => import("@/views/404.vue")
    }
  ]
});

// 添加路由守卫
router.beforeEach((to, from, next) => {
  const isLogin = localStorage.eleToken ? true : false;
  const menus = sessionStorage.getItem("menus")
    ? JSON.parse(sessionStorage.getItem("menus"))
    : [];
  if (to.path == "/login" || to.path == "/register") {
    next();
  } else if (isLogin) {
    next();
  } else if (menus.length > 0) {
    next();
  } else {
    next("/login");
  }
});

export default router;
