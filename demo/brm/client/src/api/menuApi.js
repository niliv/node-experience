import request from "@/utils/request";
const menus = {
  getMenusByIds(ids) {
    return request({
      url: "/menus/" + ids,
      method: "get"
    });
  }
};

export default menus;
