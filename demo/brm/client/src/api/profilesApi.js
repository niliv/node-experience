import request from "@/utils/request";
const profiles = {
  //登录
  getProfiles() {
    return request({
      url: "/profiles",
      method: "get"
    });
  },
  deleteProfile(profileId) {
    return request({
      url: "/profiles/delete/" + profileId,
      method: "DELETE"
    });
  },
  addProfile(profile) {
    return request({
      url: "/profiles/add",
      method: "post",
      data: profile
    });
  },
  updateProfile(profile) {
    return request({
      url: "/profiles/edit/" + profile.id,
      method: "post",
      data: profile
    });
  }
};

export default profiles;
