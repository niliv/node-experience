import request from "@/utils/request";
const roles = {
  //登录
  getRoles() {
    return request({
      url: "/roles",
      method: "get"
    });
  },
  getRolesByName(name) {
    return request({
      url: "/roles/" + name,
      method: "get"
    });
  }
};

export default roles;
