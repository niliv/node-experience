import request from "@/utils/request";
const user = {
  //登录
  login({ email, password }) {
    return request({
      url: "/users/login",
      method: "post",
      data: {
        email,
        password
      }
    });
  },
  register({ name, email, password, password2, identity }) {
    return request({
      url: "/users/register",
      method: "post",
      data: { name, email, password, password2, identity }
    });
  },
  add({ name, email, identity }) {
    return request({
      url: "/users/add",
      method: "post",
      data: { name, email, identity }
    });
  },
  updateUser(user) {
    return request({
      url: "/users/edit/" + user.id,
      method: "post",
      data: user
    });
  },
  initPwd(id) {
    return request({
      url: "/users/initPwd/" + id,
      method: "post"
    });
  },
  deleteUser(userId) {
    return request({
      url: "/users/delete/" + userId,
      method: "DELETE"
    });
  },
  deleteMany(userIds) {
    return request({
      url: "/users/delete",
      method: "DELETE",
      data: userIds
    });
  },
  getUsers() {
    return request({
      url: "/users",
      method: "get"
    });
  }
};

export default user;
