import request from "@/utils/request";
const departments = {
  //登录
  getDepartment() {
    return request({
      url: "/departments",
      method: "get"
    });
  }
};

export default departments;
