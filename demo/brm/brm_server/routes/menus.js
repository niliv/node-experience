const express = require("express");
const router = express.Router();
const passport = require("passport");
const Menu = require("../model/Menu");
const { SuccessModel, ErrorModel } = require("../model/resModel");
const { getMenuTree } = require("../utils/common");

router.get("/test", (req, res) => {
  res.json({ msg: "department works" });
});

router.post(
  "/add",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const menuFields = {};

    if (req.body.name) menuFields.name = req.body.name;
    if (req.body.icon) menuFields.icon = req.body.icon;
    if (req.body.routerName) menuFields.routerName = req.body.routerName;
    if (req.body.parent) menuFields.parent = req.body.parent;
    if (req.body.path) menuFields.path = req.body.path;
    if (req.body.component) menuFields.component = req.body.component;

    new Menu(menuFields)
      .save()
      .then(menu => {
        res.json({
          msg: new SuccessModel("添加成功"),
          menu
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("添加失败"));
      });
  }
);

router.get(
  "/:ids",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let ids = req.params.ids.split(",");
    console.log(ids);
    Menu.find({ _id: { $in: ids } })
      .then(list => {
        if (!list) {
          return res.json(new SuccessModel("没有任何内容"));
        }
        var menus = getMenuTree(list, 0);
        res.json({
          msg: new SuccessModel("获取成功"),
          menus
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("获取失败"));
      });
  }
);

module.exports = router;
