const express = require("express");
const router = express.Router();
const passport = require("passport");
const Department = require("../model/Department");
const { SuccessModel, ErrorModel } = require("../model/resModel");
const { getJsonTree } = require("../utils/common");

router.get("/test", (req, res) => {
  res.json({ msg: "department works" });
});

router.post(
  "/add",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const departmentFields = {};

    if (req.body.name) departmentFields.name = req.body.name;
    if (req.body.parent) departmentFields.parent = req.body.parent;
    if (req.body.enabled) departmentFields.enabled = req.body.enabled;

    new Department(departmentFields)
      .save()
      .then(profile => {
        res.json({
          msg: new SuccessModel("添加成功"),
          profile
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("添加失败"));
      });
  }
);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Department.find()
      .then(list => {
        if (!list) {
          return res.json(new SuccessModel("没有任何内容"));
        }
        var departments = getJsonTree(list, 0);
        res.json({
          msg: new SuccessModel("获取成功"),
          departments
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("获取失败"));
      });
  }
);

module.exports = router;
