var express = require('express')
var router = express.Router()
const User = require('../model/User')
const bcrypt = require('bcryptjs')
const gravatar = require('gravatar')
const jwt = require('jsonwebtoken')
const { SuccessModel, ErrorModel } = require('../model/resModel')
const keys = require('../config/keys')
const passport = require('passport')
const mongoose = require('mongoose')
const loginCheck = require('../middleware/loginCheck')

router.post('/register', (req, res) => {
  // 查询数据库中是否拥有邮箱
  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.json(new ErrorModel('邮箱已被注册'))
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200',
        r: 'pg',
        d: 'mm'
      })

      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        avatar,
        password: req.body.password,
        identity: req.body.identity
      })

      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err
          newUser.password = hash
          newUser
            .save()
            .then(user => res.json(new SuccessModel('注册成功: ' + user.email)))
            .catch(err => {
              console.log(err)
              res.json(new ErrorModel('注册失败'))
            })
        })
      })
    }
  })
})

router.post('/add', (req, res) => {
  // 查询数据库中是否拥有邮箱
  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      return res.json(new ErrorModel('邮箱已被注册'))
    } else {
      const avatar = gravatar.url(req.body.email, {
        s: '200',
        r: 'pg',
        d: 'mm'
      })

      const newUser = new User({
        name: req.body.name,
        email: req.body.email,
        avatar,
        password: keys.defaultPassword,
        identity: req.body.identity
      })

      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) throw err
          newUser.password = hash
          newUser
            .save()
            .then(user =>
              res.json(new SuccessModel('添加用户成功: ' + user.email))
            )
            .catch(err => {
              console.log(err)
              res.json(new ErrorModel('添加用户失败'))
            })
        })
      })
    }
  })
})

router.post(
  '/edit/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const userFields = {}

    if (req.body.name) userFields.name = req.body.name
    if (req.body.email) userFields.email = req.body.email
    if (req.body.identity) userFields.identity = req.body.identity

    User.findOneAndUpdate(
      { _id: req.params.id },
      { $set: userFields },
      { new: true }
    )
      .then(user => {
        res.json({
          msg: new SuccessModel('更新成功'),
          user
        })
      })
      .catch(err => {
        console.log(err)
        res.json(new ErrorModel('更新失败'))
      })
  }
)

router.post(
  '/initPwd/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const userFields = {}
    userFields.password = keys.defaultPassword

    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(userFields.password, salt, (err, hash) => {
        if (err) throw err
        userFields.password = hash
        User.findOneAndUpdate(
          { _id: req.params.id },
          { $set: userFields },
          { new: true }
        )
          .then(user => {
            res.json({
              msg: new SuccessModel('重置密码成功'),
              user
            })
          })
          .catch(err => {
            console.log(err)
            res.json(new ErrorModel('重置密码失败'))
          })
      })
    })
  }
)

router.delete(
  '/delete/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    User.findOneAndRemove({ _id: req.params.id })
      .then(user => {
        user.save().then(user => {
          res.json({
            msg: new SuccessModel('删除成功'),
            user
          })
        })
      })
      .catch(err => {
        console.log(err)
        res.json(new ErrorModel('删除失败'))
      })
  }
)

router.delete(
  '/delete',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    User.remove({ _id: { $in: req.body } })
      .then(item => {
        res.json({
          msg: new SuccessModel('删除成功')
        })
      })
      .catch(err => {
        console.log(err)
        res.json(new ErrorModel('删除失败'))
      })
  }
)

router.post('/login', (req, res) => {
  const email = req.body.email
  const password = req.body.password
  // 查询数据库
  User.findOne({ email })
    .then(user => {
      if (!user) {
        return res.json(new ErrorModel('用户不存在'))
      }

      // 密码匹配
      bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {
          const rule = {
            id: user.id,
            name: user.name,
            avatar: user.avatar,
            identity: user.identity
          }
          jwt.sign(
            rule,
            keys.secretOrKey,
            { expiresIn: 3600 },
            (err, token) => {
              if (err) throw err
              res.json({
                msg: new SuccessModel('登录成功'),
                token: 'Bearer ' + token
              })
            }
          )
          //res.json(res.json(new SuccessModel("登录成功")));
        } else {
          return res.json(new ErrorModel('登录失败'))
        }
      })
    })
    .catch(err => {
      console.log(err)
      res.json(new ErrorModel('网络错误'))
    })
})

router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
      identity: req.user.identity
    })
  }
)

router.get(
  '/',
  loginCheck,
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    console.log(req.niliv)
    User.find()
      .then(users => {
        if (!users) {
          return res.json(new SuccessModel('没有用户'))
        }
        res.json({
          msg: new SuccessModel('获取用户成功'),
          users
        })
      })
      .catch(err => {
        console.log(err)
        res.json(new ErrorModel('获取失败'))
      })
  }
)

router.get('/', (req, res) => {
  User.find()
    .then(users => {
      if (!users) {
        return res.json(new SuccessModel('没有用户'))
      }
      res.json({
        msg: new SuccessModel('获取用户成功'),
        users
      })
    })
    .catch(err => {
      console.log(err)
      res.json(new ErrorModel('获取失败'))
    })
})

module.exports = router
