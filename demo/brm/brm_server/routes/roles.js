const express = require("express");
const router = express.Router();
const passport = require("passport");
const Role = require("../model/Role");
const { SuccessModel, ErrorModel } = require("../model/resModel");

router.get("/test", (req, res) => {
  res.json({ msg: "role works" });
});

router.post(
  "/add",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const roleFields = {};

    if (req.body.name) roleFields.name = req.body.name;
    if (req.body.name) roleFields.menus = req.body.menus;
    if (req.body.enabled) roleFields.enabled = req.body.enabled;

    new Role(roleFields)
      .save()
      .then(role => {
        res.json({
          msg: new SuccessModel("添加成功"),
          role
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("添加失败"));
      });
  }
);

router.get(
  "/:name",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Role.findOne({ name: req.params.name })
      .then(role => {
        if (!role) {
          return res.json(new SuccessModel("没有任何内容"));
        }
        res.json({
          msg: new SuccessModel("获取成功"),
          role
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("获取失败"));
      });
  }
);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Role.find()
      .then(list => {
        if (!list) {
          return res.json(new SuccessModel("没有任何内容"));
        }
        res.json({
          msg: new SuccessModel("获取成功"),
          list
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("获取失败"));
      });
  }
);

module.exports = router;
