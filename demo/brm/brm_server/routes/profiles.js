const express = require("express");
const router = express.Router();
const passport = require("passport");
const Profile = require("../model/Profile");
const { SuccessModel, ErrorModel } = require("../model/resModel");

router.get("/test", (req, res) => {
  res.json({ msg: "profile works" });
});

router.post(
  "/add",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const profileFields = {};

    if (req.body.type) profileFields.type = req.body.type;
    if (req.body.describe) profileFields.describe = req.body.describe;
    if (req.body.income) profileFields.income = req.body.income;
    if (req.body.expend) profileFields.expend = req.body.expend;
    if (req.body.cash) profileFields.cash = req.body.cash;
    if (req.body.remark) profileFields.remark = req.body.remark;

    new Profile(profileFields)
      .save()
      .then(profile => {
        res.json({
          msg: new SuccessModel("添加成功"),
          profile
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("添加失败"));
      });
  }
);

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.find()
      .then(profile => {
        if (!profile) {
          return res.json(new SuccessModel("没有任何内容"));
        }
        res.json({
          msg: new SuccessModel("获取成功"),
          profile
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("获取失败"));
      });
  }
);

router.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOne({ _id: req.params.id })
      .then(profile => {
        if (!profile) {
          return res.json(new SuccessModel("没有任何内容"));
        }
        res.json({
          msg: new SuccessModel("获取成功"),
          profile
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("获取失败"));
      });
  }
);

router.post(
  "/edit/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const profileFields = {};

    if (req.body.type) profileFields.type = req.body.type;
    if (req.body.describe) profileFields.describe = req.body.describe;
    if (req.body.income) profileFields.income = req.body.income;
    if (req.body.expend) profileFields.expend = req.body.expend;
    if (req.body.cash) profileFields.cash = req.body.cash;
    if (req.body.remark) profileFields.remark = req.body.remark;

    Profile.findOneAndUpdate(
      { _id: req.params.id },
      { $set: profileFields },
      { new: true }
    )
      .then(profile => {
        res.json({
          msg: new SuccessModel("更新成功"),
          profile
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("更新失败"));
      });
  }
);

router.delete(
  "/delete/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Profile.findOneAndRemove({ _id: req.params.id })
      .then(profile => {
        profile.save().then(profile => {
          res.json({
            msg: new SuccessModel("删除成功"),
            profile
          });
        });
      })
      .catch(err => {
        console.log(err);
        res.json(new ErrorModel("删除失败"));
      });
  }
);

module.exports = router;
