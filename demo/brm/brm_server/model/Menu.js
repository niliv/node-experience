const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const MenuSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  icon: {
    type: String,
    required: true
  },
  routerName: {
    type: String,
    required: true
  },
  parent: {
    type: String,
    required: true
  },
  path: {
    type: String,
    required: true
  },
  component: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Menu = mongoose.model("menu", MenuSchema);
