const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const RoleSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  menus: {
    type: String
  },
  enabled: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Role = mongoose.model("role", RoleSchema);
