const env = process.env.NODE_ENV;

let mongoURI;

if (env === "dev") {
  mongoURI = "mongodb://brm_niliv:123456@118.24.175.34:27017/brm";
}

if (env === "production") {
  mongoURI = "mongodb://brm_niliv:123456@118.24.175.34:27017/brm";
}

module.exports = {
  mongoURI
};
