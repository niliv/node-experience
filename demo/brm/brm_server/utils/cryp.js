const crypto = require("crypto");

const key = "asfasfasas!@#$%^&*()_+";

function md5(str) {
  let obj = crypto.createHash("md5");
  obj.update(str);
  return obj.digest("hex");
}

function genPassword(password) {
  const str = `password=${password}&key=${key}`;
  return md5(str);
}

module.exports = {
  genPassword
};
