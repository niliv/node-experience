const express = require('express')
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const db = require("./config/keys").mongoURI
const users = require('./routes/api/users')
const profile = require('./routes/api/profile')
const posts = require('./routes/api/posts')
const passport = require('passport')
const app = express();

//bodyParser
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())


//连接数据库
mongoose.connect(db)
        .then(()=>console.log("MongoDB Connected"))
        .catch(err=>console.log(err))

//跨域
app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Content-Type");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    next();
})


//passport
app.use(passport.initialize());
require('./config/passport')(passport)


//路由
app.use("/api/users",users)
app.use("/api/profile",profile)
app.use("/api/posts",posts)


const port = process.env.PORT || 5000
app.listen(port,()=>{
    console.log(`Server running on ${port}`)
})