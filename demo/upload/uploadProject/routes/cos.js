var express = require("express");
var router = express.Router();
const { SuccessModel, ErrorModel } = require("../model/resModel");
const multer = require("multer");
const fs = require("fs");
var COS = require("cos-nodejs-sdk-v5");

var cos = new COS({
  AppId: "1252830662",
  SecretId: "AKIDYtRkHmu2auAbgukcDaPyIExVt64Atkwt",
  SecretKey: "tYljHdfTCtgoeOddNOOIdkryuUoshFYy"
});
var tengxun_cos = {
  Bucket: "mytest-1252830662",
  Region: "ap-chengdu"
};

var uploadDir = "upload/";
var upload = multer({
  dest: uploadDir
});

router.get("/test", function(req, res, next) {
  res.json({
    msg: "file works"
  });
});

router.all("/upload", upload.single("clouds"), function(req, res, next) {
  // 文件路径
  var filePath = "./" + req.file.path;
  // 文件类型
  var temp = req.file.originalname.split(".");
  var fileType = temp[temp.length - 1];
  var lastName = "." + fileType;
  // 构建图片名
  var fileName = Date.now() + lastName;
  // 图片重命名
  fs.rename(filePath, fileName, err => {
    if (err) {
      res.end(JSON.stringify({ status: "102", msg: "文件写入失败" }));
    } else {
      var localFile = "./" + fileName;
      var key = fileName;

      // 腾讯云 文件上传
      var params = {
        Bucket: tengxun_cos.Bucket /* 必须 */,
        Region: tengxun_cos.Region /* 必须 */,
        Key: key /* 必须 */,
        FilePath: localFile /* 必须 */
      };
      cos.sliceUploadFile(params, function(err, data) {
        if (err) {
          fs.unlinkSync(localFile);
          res.end(
            JSON.stringify({
              status: "101",
              msg: "上传失败",
              error: JSON.stringify(err)
            })
          );
        } else {
          fs.unlinkSync(localFile);
          var imageSrc =
            "https://mytest-1252830662.cos.ap-chengdu.myqcloud.com/" + data.Key;
          res.end(
            JSON.stringify({
              status: "100",
              msg: "上传成功",
              imageUrl: imageSrc
            })
          );
        }
      });
    }
  });
});

module.exports = router;
