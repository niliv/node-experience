var express = require("express");
var router = express.Router();
const { SuccessModel, ErrorModel } = require("../model/resModel");
const multer = require("multer");
const fs = require("fs");

//设置文件上传的路径
var uploadDir = "upload/";
//规定只上传单个文件 使用single
var upload = multer({
  dest: uploadDir
}).single("files");
//规定只上传多个文件 使用array
var uploadMultiple = multer({ dest: uploadDir }).array("files");

router.get("/test", function(req, res, next) {
  res.json({
    msg: "file works"
  });
});
//上传单个文件
router.post("/upload", function(req, res, next) {
  //文件上传
  upload(req, res, function(err) {
    if (err) {
      console.error(err.message);
    } else {
      //获取文件的名称，然后拼接成将来要存储的文件路径
      var des_file = uploadDir + req.file.originalname;
      //读取临时文件
      fs.readFile(req.file.path, function(err, data) {
        //将data写入文件中，写一个新的文件
        fs.writeFile(des_file, data, function(err) {
          if (err) {
            console.error(err.message);
          } else {
            var reponse = {
              message: "File uploaded successfully",
              filename: req.file.originalname
            };
            //删除临时文件
            fs.unlink(req.file.path, function(err) {
              if (err) {
                console.error(err.message);
              } else {
                console.log("delete " + req.file.path + " successfully!");
              }
            });
          }
          res.end(JSON.stringify(reponse));
        });
      });
    }
  });
});
//上传多个文件
router.post("/uploadMultiple", function(req, res, next) {
  console.log(1);
  //多个文件上传
  uploadMultiple(req, res, function(err) {
    if (err) {
      console.log(2);
      console.error("[System] " + err.message);
    } else {
      console.log(3);
      //循环处理
      var fileCount = req.files.length;
      req.files.forEach(function(i) {
        //设置存储的文件路径
        var uploadFilePath = uploadDir + i.originalname;
        //获取临时文件的存储路径
        var uploadTmpPath = i.path;
        //读取临时文件
        fs.readFile(uploadTmpPath, function(err, data) {
          if (err) {
            console.error("[System] " + err.message);
          } else {
            //读取成功将内容写入到上传的路径中，文件名为上面构造的文件名
            fs.writeFile(uploadFilePath, data, function(err) {
              if (err) {
                console.error("[System] " + err.message);
              } else {
                //写入成功,删除临时文件
                fs.unlink(uploadTmpPath, function(err) {
                  if (err) {
                    console.error("[System] " + err.message);
                  } else {
                    console.log(
                      "[System] " + "Delete " + uploadTmpPath + " successfully!"
                    );
                  }
                });
              }
            });
          }
        });
      });

      //所有文件上传成功
      //回复信息
      var reponse = {
        message: "File uploaded successfully"
      };
      //返回
      res.end(JSON.stringify(reponse));
    }
  });
});

module.exports = router;
