var getJsonTree = function(data, parentId) {
  var itemArr = [];
  for (var i = 0; i < data.length; i++) {
    var node = data[i];
    if (node.parent == parentId) {
      var newNode = {};
      newNode.id = node._id;
      newNode.name = node.name;
      newNode.child = getJsonTree(data, node._id);
      itemArr.push(newNode);
    }
  }
  return itemArr;
};

module.exports = {
  getJsonTree
};
