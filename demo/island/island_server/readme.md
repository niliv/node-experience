# koa

node不支持 ES6的 `import from`，可以用babel，或者Typescript

**中间件**

```javascript
const Koa = require('koa')

const app = new Koa()

app.use((ctx, next) => {
  console.log('1')
  next()
})
app.use((ctx, next) => {
  console.log('2')
})

app.listen(4000)
```

**洋葱模型**

```javascript
const Koa = require('koa')

const app = new Koa()

app.use(async (ctx, next) => {
  console.log('1')
  await next()
  console.log('2')
})
app.use(async (ctx, next) => {
  console.log('3')
  await next()
  console.log('4')
})

app.listen(4000)
//1,3,4,2
```

use里的函数中间件始终返回都是promise

**async await**

```javascript
const Koa = require('koa')
const app = new Koa()

//await对表达式求值
app.use(async (ctx, next) => {
  const axios = require('axios')
  const res = (await 10) * 10
  console.log(res)
})

//await阻塞异步函数 不会马上执行后面，等待返回
app.use(async (ctx, next) => {
  const axios = require('axios')
  const res = await axios.get('http://7yue.pro')
  console.log(res)
})

app.listen(3000)
```

**保证洋葱圈模型，每个中间件必须加async，next前必须加await**

**ctx可以保存上下文，可以在中间件传值，但必须保证洋葱模型**

```javascript
const Koa = require('koa')
const app = new Koa()

//await对表达式求值
app.use(async (ctx, next) => {
  await next()
  console.log('1', ctx.r)
})

//await阻塞异步函数
app.use(async (ctx, next) => {
  const axios = require('axios')
  const res = (await 10) * 10
  ctx.r = res
  console.log('2', res)
})

app.listen(3000)
//2,100
//1,100
```

**vscode launch.json** nodemon在vscode启动调试

```json
//package
  "scripts": {
    "start:dev": "nodemon --inspect-brk",
    "start:prod": "node app.js",
    "debug": "nodemon --inspect-brk app.js"
  },

{
  // 使用 IntelliSense 了解相关属性。 
  // 悬停以查看现有属性的描述。
  // 欲了解更多信息，请访问: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [{
      "name": "Launch via NPM",
      "type": "node",
      "request": "launch",
      "cwd": "${workspaceFolder}",
      "runtimeExecutable": "npm",
      "runtimeArgs": [
        "run-script", "debug"
      ],
      "port": 9229,
      "console": "integratedTerminal",
      "internalConsoleOptions": "neverOpen"
    },

    {
      "type": "node",
      "request": "launch",
      "name": "启动程序",
      "program": "${workspaceFolder}\\app.js"
    },
    {
      "type": "node",
      "request": "launch",
      "name": "当前文件",
      "program": "${file}"
    },
  ]
}
```

**路由自动加载** api分版本

```javascript
const Koa = require('koa')
const Router = require('koa-router')
const requireDirectory = require('require-directory')
// const classic = require('./api/v1/classic')
// const book = require('./api/v1/book')

const app = new Koa()

requireDirectory(module, './api', { visit: whenLoadModule })
function whenLoadModule(obj) {
  if (obj instanceof Router) {
    app.use(obj.routes())
  }
}

// app.use(classic.routes())
// app.use(book.routes())

app.listen(3000)
```

**将路由加载分离出来**

```javascript
const Router = require('koa-router')
const requireDirectory = require('require-directory')

class InitManager {
  static initCore(app) {
    InitManager.app = app
    InitManager.initLoadRounters()
  }

  static initLoadRounters() {
    const apiDirectory = `${process.cwd()}/app/api`
    requireDirectory(module, apiDirectory, { visit: whenLoadModule })
    function whenLoadModule(obj) {
      if (obj instanceof Router) {
        InitManager.app.use(obj.routes())
      }
    }
  }
}

module.exports = InitManager
```

```javascript
const Koa = require('koa')
const InitManager = require('./core/init')

const app = new Koa()

InitManager.initCore(app)

app.listen(3000)

```

**参数**

```javascript
const Router = require('koa-router')
const router = new Router()

router.post('/v1/:id/classic/latest', (ctx, next) => {
  const path = ctx.params //rest
  const query = ctx.request.query //query
  const headers = ctx.request.header //header
  const body = ctx.request.body //body
  console.log(ctx)
  ctx.body = { key: 'classic' }
})

module.exports = router
```

```javascript
//app.js
const parser = require('koa-bodyparser')
app.use(parser())
```

**异步异常** await接受promise，如果是reject就会扑捉到异常

```javascript
async function func1() {
  try {
    await func2()
  } catch (error) {
    console.log('error1')
  }
}

function func2() {
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      const r = Math.random()
      if (r < 0.5) {
        reject('eroor')
      }
    }, 1000)
  })
}
```

**异常中间件**

```javascript
const catchError = async (ctx, next) => {
  try {
    await next()
  } catch (error) {
    ctx.body = { error: 'sorry api exception' }
  }
}

module.exports = catchError
```

```javascript
const Koa = require('koa')
const parser = require('koa-bodyparser')
const InitManager = require('./core/init')
const catchError = require('./middlewares/exception')

const app = new Koa()
console.log(1111)
app.use(parser())
app.use(catchError)
InitManager.initCore(app)

app.listen(3000)
```

```javascript
//api
const Router = require('koa-router')
const router = new Router()

router.post('/v1/:id/classic/latest', (ctx, next) => {
  const path = ctx.params //rest
  const query = ctx.request.query //query
  const headers = ctx.request.header //header
  const body = ctx.request.body //body
  console.log(111)
  ctx.body = { key: 'classic' }
  console.log(222)
  throw new Error('api error')
  console.log(333)  //不会执行
})

module.exports = router

//111
//222
//sorry api exception
```

**use顺序执行**

```javascript
const Koa = require('koa')
const parser = require('koa-bodyparser')
const InitManager = require('./core/init')
const catchError = require('./middlewares/exception')
const test = require('./middlewares/test')

const app = new Koa()
console.log(1111)
app.use(parser())
app.use(catchError)
app.use(test)
InitManager.initCore(app)

app.listen(3000)

//middle test
//111
//222
//error
```

**自定义异常处理**

```javascript
//init
const Router = require('koa-router')
const requireDirectory = require('require-directory')

class InitManager {
  static initCore(app) {
    InitManager.app = app
    InitManager.initLoadRounters()
    InitManager.loadHttpException()
  }

  static initLoadRounters() {
    const apiDirectory = `${process.cwd()}/app/api`
    requireDirectory(module, apiDirectory, { visit: whenLoadModule })
    function whenLoadModule(obj) {
      if (obj instanceof Router) {
        InitManager.app.use(obj.routes())
      }
    }
  }

  static loadHttpException() {
    const errors = require('./http-exception')
    global.errs = errors
  }
}

module.exports = InitManager

```

```javascript
//http-exception
class HttpException extends Error {
  constructor(msg = '服务器异常', errorCode = 10000, code = 400) {
    super()
    this.errorCode = errorCode
    this.code = code
    this.msg = msg
  }
}

class ParameterException extends HttpException {
  constructor(msg, errorCode) {
    super()
    this.code = 400
    this.msg = msg || '参数错误'
    this.errorCode = errorCode || 10000
  }
}

module.exports = {
  HttpException,
  ParameterException
}

```

```javascript
//api
const Router = require('koa-router')
const router = new Router()

router.post('/api/v1/:id/classic/latest', (ctx, next) => {
  const path = ctx.params //rest
  const query = ctx.request.query //query
  const headers = ctx.request.header //header
  const body = ctx.request.body //body

  if (true) {
    const error = new global.errs.ParameterException()
    throw error
  }

  ctx.body = { key: 'classic' }
})

module.exports = router

```

```javascript
//middle
const { HttpException } = require('../core/http-exception')
const catchError = async (ctx, next) => {
  try {
    await next()
  } catch (error) {
    if (error instanceof HttpException) { //自己抛出的
      ctx.body = {
        msg: error.msg,
        error_code: error.errorCode,
        request: `${ctx.method} ${ctx.path}`
      }
      ctx.status = error.code
    } else { //未知异常
      ctx.body = {
        msg: 'mistake',
        error_code: 999,
        request: `${ctx.method} ${ctx.path}`
      }
      ctx.status = 500
    }
  }
}

module.exports = catchError

```

**验证参数**

lin-validator.js util.js http-exception.js 放入core

```javascript
//validator.js
const { LinValidator, Rule } = require('../../core/lin-validator')
class PositiveIntegerValidator extends LinValidator {
  constructor() {
    super()
    this.id = [
      new Rule('isInt', '需要是正整数', {
        min: 1
      })
    ]
  }
}

module.exports = {
  PositiveIntegerValidator
}

```

```javascript
//api
const Router = require('koa-router')
const router = new Router()
const { PositiveIntegerValidator } = require('../../validator/validator')

router.post('/api/v1/:id/classic/latest', (ctx, next) => {
  const path = ctx.params //rest
  const query = ctx.request.query //query
  const headers = ctx.request.header //header
  const body = ctx.request.body //body

  const v = new PositiveIntegerValidator().validate(ctx)
  const id = v.get('path.id') //获取参数 自动转化数据类型
  // const id = v.get('path.id',parsed=false)

  ctx.body = { key: 'classic success' }
})

module.exports = router

```

**生产环境抛出未知异常**

```javascript
const { HttpException } = require('../core/http-exception')

const catchError = async (ctx, next) => {
  try {
    await next()
  } catch (error) {
    // 开发环境
    // 生产环境
    // 开发环境 不是HttpException
    const isHttpException = error instanceof HttpException
    const isDev = global.config.environment === 'dev'

    if (isDev && !isHttpException) {
      throw error
    }

    if (isHttpException) {
      ctx.body = {
        msg: error.msg,
        error_code: error.errorCode,
        request: `${ctx.method} ${ctx.path}`
      }
      ctx.status = error.code
    } else {
      ctx.body = {
        msg: 'we made a mistake O(∩_∩)O~~',
        error_code: 999,
        request: `${ctx.method} ${ctx.path}`
      }
      ctx.status = 500
    }
  }
}

module.exports = catchError
```

```javascript
//config
module.exports = {
  environment: 'dev'
}

```

```javascript
const Router = require('koa-router')
const requireDirectory = require('require-directory')

class InitManager {
  static initCore(app) {
    InitManager.app = app
    InitManager.initLoadRounters()
    InitManager.loadHttpException()
    InitManager.loadConfig()
  }
  static loadConfig(path = '') {
    const configPath = path || process.cwd() + '/config/config.js'
    const config = require(configPath)
    global.config = config
  }
  static initLoadRounters() {
    const apiDirectory = `${process.cwd()}/app/api`
    requireDirectory(module, apiDirectory, { visit: whenLoadModule })
    function whenLoadModule(obj) {
      if (obj instanceof Router) {
        InitManager.app.use(obj.routes())
      }
    }
  }

  static loadHttpException() {
    const errors = require('./http-exception')
    global.errs = errors
  }
}

module.exports = InitManager

```

**sequelize**

https://itbilu.com/nodejs/npm/VkYIaRPz-.html#api-instance-method

```javascript
module.exports = {
  environment: 'dev',
  database: {
    dbName: 'island',
    host: '118.24.175.34',
    port: 3306,
    user: 'root',
    password: 'p@ssw0rd'
  }
}
```

```javascript
const Sequelize = require('sequelize')
const {
  dbName,
  host,
  port,
  user,
  password
} = require('../config/config').database

const sequelize = new Sequelize(dbName, user, password, {
  dialect: 'mysql',
  host,
  port,
  logging: true,
  timezone: '+08:00',
  define: {
    //create_time  update_time delete_time
    timestamps: true, //为模型添加 createdAt 和 updatedAt 两个时间戳字段
    //使用逻辑删除。设置为true后，调用 destroy 方法时将不会删队模型，而是设置一个 deletedAt 列。此设置需要 timestamps=true
    paranoid: true,
    createdAt: 'created_at', //如果为字符串，则使用提供的值代替 createdAt 列的默认名，设置为flase则不添加这个字段
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    underscored: true, //换列名的驼峰命名规则为下划线命令规则
    freezeTableName: true, //设置为true时，sequelize不会改变表名，否则可能会按其规则有所调整
    scopes: {
      bh: {
        attributes: {
          exclude: ['updated_at', 'deleted_at', 'created_at']
        }
      }
    }
  }
})

sequelize.sync({
  force: false //设置为 true，会在创建表前先删除原表
})

module.exports = {
  sequelize
}

```

```javascript
const Koa = require('koa')
const parser = require('koa-bodyparser')
const InitManager = require('./core/init')
const catchError = require('./middlewares/exception')

const app = new Koa()
require('./app/models/user')
console.log(1111)
app.use(parser())
app.use(catchError)
InitManager.initCore(app)

app.listen(3000)

```

**用户注册参数校验**

```javascript
const { LinValidator, Rule } = require('../../core/lin-validator')
class PositiveIntegerValidator extends LinValidator {
  constructor() {
    super()
    this.id = [
      new Rule('isInt', '需要是正整数', {
        min: 1
      })
    ]
  }
}

class RegisterValidator extends LinValidator {
  constructor() {
    super()
    this.email = [new Rule('isEmail', '不符合Email规范')]
    this.password1 = [
      // 用户指定范围 123456 $^
      new Rule('isLength', '密码至少6个字符，最多32个字符', {
        min: 6,
        max: 32
      }),
      new Rule(
        'matches',
        '密码不符合规范',
        '^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]'
      )
    ]
    this.password2 = this.password1
    this.nickname = [
      new Rule('isLength', '昵称不符合长度规范', {
        min: 4,
        max: 32
      })
    ]
  }

  validatePassword(vals) {
    const psw1 = vals.body.password1
    const psw2 = vals.body.password2
    if (psw1 !== psw2) {
      throw new Error('两个密码必须相同')
    }
  }

  async validateEmail(vals) {
    const email = vals.body.email
    const user = await User.findOne({
      where: {
        email: email
      }
    })
    if (user) {
      throw new Error('email已存在')
    }
  }
}

module.exports = {
  PositiveIntegerValidator,
  RegisterValidator
}

```

```javascript
//user.js
const Router = require('koa-router')
const router = new Router({ prefix: '/api/v1/user' }) //前缀
const { RegisterValidator } = require('../../validator/validator')

router.post('/register', async ctx => {
  const v = new RegisterValidator().validate(ctx)
})

module.exports = router
```

**用户注册**

```javascript
const Router = require('koa-router')
const router = new Router({ prefix: '/api/v1/user' }) //前缀
const { RegisterValidator } = require('../../validator/validator')
const { User } = require('../../models/user')

router.post('/register', async ctx => {
  const v = await new RegisterValidator().validate(ctx)

  const user = {
    email: v.get('body.email'),
    password: v.get('body.password2'),
    nickname: v.get('body.nickname')
  }

  await User.create(user)
  throw new global.errs.Success()
})

module.exports = router
```

**用户登录**

```javascript
//类型枚举

function isThisType(val){
    for(let key in this){
        if(this[key] === val){
            return true
        }
    }
    return false
}

const LoginType = {
    USER_MINI_PROGRAM:100,
    USER_EMAIL:101,
    USER_MOBILE:102,
    ADMIN_EMAIL:200,
    isThisType
}

const ArtType = {
    MOVIE:100,
    MUSIC:200,
    SENTENCE:300,
    BOOK:400,
    isThisType
}

module.exports = {
    LoginType,
    ArtType
}
```

```javascript
//token.js
const Router = require('koa-router')
const {
  TokenValidator,
  NotEmptyValidator
} = require('../../validators/validator')
const { LoginType } = require('../../lib/enum')
const { User } = require('../../models/user')

const { WXManager } = require('../../services/wx')

const { generateToken } = require('../../../core/util')
const { Auth } = require('../../../middlewares/auth')

const router = new Router({
  prefix: '/api/v1/token'
})

router.post('/', async ctx => {
  const v = await new TokenValidator().validate(ctx)
  let token
  switch (v.get('body.type')) {
    case LoginType.USER_EMAIL:
      token = await emailLogin(v.get('body.account'), v.get('body.secret'))
      break
    case LoginType.USER_MINI_PROGRAM:
      token = await WXManager.codeToToken(v.get('body.account'))
      break
    case LoginType.ADMIN_EMAIL:
      break
    default:
      throw new global.errs.ParameterException('没有相应的处理函数')
  }
  ctx.body = {
    token
  }
})

router.post('/verify', async ctx => {
  // token
  const v = await new NotEmptyValidator().validate(ctx)
  const result = Auth.verifyToken(v.get('body.token'))
  ctx.body = {
    is_valid: result
  }
})

async function emailLogin(account, secret) {
  const user = await User.verifyEmailPassword(account, secret)
  return (token = generateToken(user.id, Auth.USER))
}

module.exports = router

```

```javascript
const bcrypt = require('bcryptjs')

const {
    sequelize
} = require('../../core/db')


const {
    Sequelize,
    Model
} = require('sequelize')

// define
class User extends Model {
    static async verifyEmailPassword(email, plainPassword) {
        const user = await User.findOne({
            where: {
                email
            }
        })
        if (!user) {
            throw new global.errs.AuthFailed('账号不存在')
        }
        // user.password === plainPassword
        const correct = bcrypt.compareSync(
            plainPassword, user.password)
        if(!correct){
            throw new global.errs.AuthFailed('密码不正确')
        }
        return user
    }

    static async getUserByOpenid(openid){
        const user = await User.findOne({
            where:{
                openid
            }
        })
        return user
    }

    static async registerByOpenid(openid) {
        return await User.create({
            openid
        })
    }
}

User.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nickname: Sequelize.STRING,
    email: {
        type: Sequelize.STRING(128),
        unique: true
    },
    password: {
        //扩展 设计模式 观察者模式
        //ES6 Reflect Vue3.0 
        type: Sequelize.STRING,
        set(val) {
            const salt = bcrypt.genSaltSync(10)
            const psw = bcrypt.hashSync(val, salt)
            this.setDataValue('password', psw)
        }
    },
    openid: {
        type: Sequelize.STRING(64),
        unique: true
    },
}, {
    sequelize,
    tableName: 'user'
})

module.exports = {
    User
}

// 数据迁移 SQL 更新 风险
```

```javascript
//util
const generateToken = function(uid, scope){
    const secretKey = global.config.security.secretKey
    const expiresIn = global.config.security.expiresIn
    const token = jwt.sign({
        uid,
        scope
    },secretKey,{
        expiresIn
    })
    return token
}
```

```javascript
//wx
const util = require('util')
const axios = require('axios')

const {User} = require('../models/user')
const { generateToken } = require('../../core/util')
const { Auth } = require('../../middlewares/auth')

class WXManager {

    static async codeToToken(code) {
        const url = util.format(global.config.wx.loginUrl,
            global.config.wx.appId,
            global.config.wx.appSecret,
            code)

        const result = await axios.get(url)
        if (result.status !== 200) {
            throw new global.errs.AuthFailed('openid获取失败')
        }
        const errcode = result.data.errcode
        const errmsg = result.data.errmsg
        if (errcode){
            throw new global.errs.AuthFailed('openid获取失败:'+errmsg)
        }
        // openid
        // 档案 user uid openid 长
        // openid 

        let user = await User.getUserByOpenid(result.data.openid)
        if(!user){
            user = await User.registerByOpenid(result.data.openid)
        }
        return generateToken(user.id, Auth.USER)
    }

}

module.exports = {
    WXManager
}
```

**判断token和权限**

```javascript
const basicAuth = require('basic-auth')
const jwt = require('jsonwebtoken')

class Auth {
  constructor(level) {
    this.level = level || 1
    Auth.USER = 8
    Auth.ADMIN = 16
    Auth.SUPER_ADMIN = 32
  }

  get m() {
    return async (ctx, next) => {
      const userToken = basicAuth(ctx.req)
      let errMsg = 'token不合法'

      if (!userToken || !userToken.name) {
        throw new global.errs.Forbbiden(errMsg)
      }
      try {
        var decode = jwt.verify(
          userToken.name,
          global.config.security.secretKey
        )
      } catch (error) {
        if (error.name == 'TokenExpiredError') {
          errMsg = 'token已过期'
        }
        throw new global.errs.Forbbiden(errMsg)
      }

      if (decode.scope < this.level) {
        errMsg = '权限不足'
        throw new global.errs.Forbbiden(errMsg)
      }

      // uid,scope
      ctx.auth = {
        uid: decode.uid,
        scope: decode.scope
      }

      await next()
    }
  }

  static verifyToken(token) {
    try {
      jwt.verify(token, global.config.security.secretKey)
      return true
    } catch (error) {
      return false
    }
  }
}

module.exports = {
  Auth
}

```

```javascript
const Router = require('koa-router')
const router = new Router({ prefix: '/api/v1/classic' }) //前缀
const { PositiveIntegerValidator } = require('../../validator/validator')
const { Auth } = require('../../../middlewares/auth')

router.get('/latest', new Auth(9).m, async (ctx, next) => {
  console.log(ctx.auth)
  ctx.body = ctx.auth.uid
  // const path = ctx.params //rest
  // const query = ctx.request.query //query
  // const headers = ctx.request.header //header
  // const body = ctx.request.body //body
  // const v = await new PositiveIntegerValidator().validate(ctx)
  // const id = v.get('path.id') //获取参数 自动转化数据类型
  // // const id = v.get('path.id',parsed=false)
  // ctx.body = { key: 'classic success' }
})

module.exports = router
```

**事务**

```javascript
const {
    sequelize
} = require('../../core/db')
const {
    Sequelize,
    Model,
    Op
} = require('sequelize')
const {
    Art
} = require('./art')

class Favor extends Model {
    // 业务表
    static async like(art_id, type, uid) {
        const favor = await Favor.findOne({
            where: {
                art_id,
                type,
                uid
            }
        })
        if (favor) {
            throw new global.errs.LikeError()
        }
        return sequelize.transaction(async t => {
            await Favor.create({
                art_id,
                type,
                uid
            }, {
                transaction: t
            })
            const art = await Art.getData(art_id, type, false)
            await art.increment('fav_nums', {
                by: 1,
                transaction: t
            })
        })
    }

    static async disLike(art_id, type, uid) {
        const favor = await Favor.findOne({
            where: {
                art_id,
                type,
                uid
            }
        })
        if (!favor) {
            throw new global.errs.DislikeError()
        }
        // Favor 表 favor 记录
        return sequelize.transaction(async t => {
            await favor.destroy({
                force: true,
                transaction: t
            })
            const art = await Art.getData(art_id, type, false)
            await art.decrement('fav_nums', {
                by: 1,
                transaction: t
            })
        })
    }

    static async userLikeIt(art_id, type, uid) {
        const favor = await Favor.findOne({
            where: {
                uid,
                art_id,
                type,
            }
        })
        return favor ? true : false
    }

    static async getMyClassicFavors(uid) {
        const arts = await Favor.findAll({
            where: {
                uid,
                type:{
                    [Op.not]:400,
                }
            }
        })
        if(!arts){
            throw new global.errs.NotFound()
        }
       
        return await Art.getList(arts)
    }

    static async getBookFavor(uid, bookID){
        const favorNums = await Favor.count({
            where: {
                art_id:bookID,
                type:400
            }
        })
        const myFavor = await Favor.findOne({
            where:{
                art_id:bookID,
                uid,
                type:400
            }
        })
        return {
            fav_nums:favorNums,
            like_status:myFavor?1:0
        }
    }
}

Favor.init({
    uid: Sequelize.INTEGER,
    art_id: Sequelize.INTEGER,
    type: Sequelize.INTEGER
}, {
    sequelize,
    tableName: 'favor'
})

module.exports = {
    Favor
}


```



**处理静态文件**

```javascript
const static = require('koa-static')
app.use(static(path.join(__dirname, './static')))
```

