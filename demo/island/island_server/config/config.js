module.exports = {
  environment: 'dev',
  database: {
    dbName: 'island',
    host: '118.24.175.34',
    port: 3306,
    user: 'root',
    password: 'p@ssw0rd'
  },
  security: {
    secretKey: 'abcdefg',
    expiresIn: 60 * 20
  },
  wx: {
    appId: 'wxe426a794c4a43599',
    appSecret: '148559456542f34ad50b0216b0146834',
    loginUrl:
      'https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code'
  }
}
