const Router = require('koa-router')
const router = new Router()

router.get('/api/v1/book/latest', (ctx, next) => {
  ctx.body = { key: 'book' }
})

module.exports = router
