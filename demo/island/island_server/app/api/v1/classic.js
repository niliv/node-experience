const Router = require('koa-router')
const router = new Router({ prefix: '/api/v1/classic' }) //前缀
const { PositiveIntegerValidator } = require('../../validator/validator')
const { Auth } = require('../../../middlewares/auth')
const { Flow } = require('../../models/flow')
const { Art } = require('../../models/art')

router.get('/latest', new Auth().m, async (ctx, next) => {
  const flow = await Flow.findOne({
    order: [['index', 'DESC']]
  })
  const art = await Art.getData(flow.art_id, flow.type)
  // const i = art.get('image')
  // const t = art.image
  // const s = art.getDataValue('image')
  // const likeLatest = await Favor.userLikeIt(
  //   flow.art_id,
  //   flow.type,
  //   ctx.auth.uid
  // )
  art.setDataValue('index', flow.index)
  //art.setDataValue('like_status', likeLatest)
  ctx.body = art
})

module.exports = router
