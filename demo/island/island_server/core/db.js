const Sequelize = require('sequelize')
const {
  dbName,
  host,
  port,
  user,
  password
} = require('../config/config').database

const sequelize = new Sequelize(dbName, user, password, {
  dialect: 'mysql',
  host,
  port,
  logging: true,
  timezone: '+08:00',
  define: {
    //create_time  update_time delete_time
    timestamps: true, //为模型添加 createdAt 和 updatedAt 两个时间戳字段
    //使用逻辑删除。设置为true后，调用 destroy 方法时将不会删队模型，而是设置一个 deletedAt 列。此设置需要 timestamps=true
    paranoid: true,
    createdAt: 'created_at', //如果为字符串，则使用提供的值代替 createdAt 列的默认名，设置为flase则不添加这个字段
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    underscored: true, //换列名的驼峰命名规则为下划线命令规则
    freezeTableName: true, //设置为true时，sequelize不会改变表名，否则可能会按其规则有所调整
    scopes: {
      bh: {
        attributes: {
          exclude: ['updated_at', 'deleted_at', 'created_at']
        }
      }
    }
  }
})

sequelize.sync({
  force: false //设置为 true，会在创建表前先删除原表
})

module.exports = {
  sequelize
}
