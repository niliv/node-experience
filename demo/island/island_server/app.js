const Koa = require('koa')
const parser = require('koa-bodyparser')
const InitManager = require('./core/init')
const catchError = require('./middlewares/exception')

const app = new Koa()
console.log(1111)
app.use(parser())
app.use(catchError)
InitManager.initCore(app)

app.listen(3000)
