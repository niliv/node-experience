async function func1() {
  try {
    await func2()
  } catch (error) {
    console.log('error1')
  }
}

function func2() {
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      const r = Math.random()
      if (r < 0.5) {
        reject('eroor')
      }
    }, 1000)
  })
}
