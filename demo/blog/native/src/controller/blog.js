const {
  exec,
  escape
} = require('../db/mysql')
const
  xss = require('xss')

const getList = (author, keyword) => {
  let sql = `select * from blogs where 1=1`
  if (author) {
    author = escape(xss(author))
    sql += ` and author=${author}`
  }
  if (keyword) {
    keyword = escape(keyword)
    sql += ` and title like %${keyword}%`
  }
  sql += ` order by createtime desc`;
  return exec(sql);
}

const getDetail = (id) => {
  let sql = `select * from blogs where id='${id}'`
  return exec(sql).then(row => {
    return row[0]
  });
}

const newBlog = (blogData = {}) => {

  blogData.title = escape(xss(blogData.title))
  blogData.content = escape(xss(blogData.content))
  blogData.author = escape(xss(blogData.author))
  blogData.createTime = Date.now()

  let sql = `insert into blogs (title,content,createTime,author) values (${blogData.title},${blogData.content},${blogData.createTime},${blogData.author})`
  return exec(sql).then(insertData => {
    return {
      id: insertData.insertId
    }
  })
}

const updateBlog = (id, blogData = {}) => {
  id = escape(id)
  blogData.title = escape(xss(blogData.title))
  blogData.content = escape(xss(blogData.content))
  blogData.createTime = Date.now()

  let sql = `update blogs set title=${blogData.title},content=${blogData.content},createTime=${blogData.createTime} where id=${id}`
  console.log(sql)
  return exec(sql).then(updateData => {
    if (updateData.affectedRows > 0) {
      return true;
    }
    return false;
  })
}

const delBlog = (id, author) => {
  id = escape(id)
  author = escape(xss(author))
  let sql = `delete from  blogs where id=${id} and author=${author}`
  return exec(sql).then(deleteData => {
    if (deleteData.affectedRows > 0) {
      return true;
    }
    return false;
  })
}

module.exports = {
  getList,
  getDetail,
  newBlog,
  updateBlog,
  delBlog
};