const {
  login
} = require('../controller/user')
const {
  SuccessModel,
  ErrorModel
} = require('../model/resModel')
const {
  set
} = require('../db/redis')


const handleUserRouter = (req, res) => {
  const method = req.method
  if (method === 'POST' && req.path === '/api/user/login') {
    const {
      username,
      password
    } = req.body
    const result = login(username, password)
    return result.then(data => {
      if (data.username) {
        req.session.username = data.username
        req.session.realname = data.realname
        set(req.sessionId, req.session)
        console.dir('user req.session: ' + JSON.stringify(req.session))
        return new SuccessModel()
      } else {
        return new ErrorModel('登录失败')
      }
    })

  }

  if (method === 'GET' && req.path === '/api/user/login-test') {
    console.dir('req.session: ' + JSON.stringify(req.session))
    if (req.session.username) {
      return Promise.resolve(new SuccessModel({
        session: req.session
      }))
    }
    return Promise.resolve(new ErrorModel('未登录'))
  }
}

module.exports = handleUserRouter