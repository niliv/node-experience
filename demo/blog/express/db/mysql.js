const mysql = require('mysql')
const {
  MYSQL_CONF
} = require('../conf/db')

var connection = mysql.createConnection(MYSQL_CONF)

connection.connect((err) => {
  if (err) throw err;
  console.log('mysql is connecting')
});

function exec(sql) {
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, result) => {
      if (err) {
        reject(err)
      } else {
        resolve(result)
      }
    })
  })
}

module.exports = {
  exec,
  escape: mysql.escape
}