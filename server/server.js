/* web服务器
1. 返回文件
2. 数据交互（GET\POST）
3. 数据库 */

/**处理get请求 */
/* const http = require('http')
const querystring = require('querystring')

http.createServer((req, res) => {
  console.log(req.method)
  const url = req.url
  req.query = querystring.parse(url.split('?')[1])
  console.log(req.query)
  res.end(JSON.stringify(req.query))
}).listen(8000) */

/**处理post请求 */
const http = require('http')
const querystring = require('querystring')

http.createServer((req, res) => {

  console.log(req.method)
  console.log(req.headers['content-type'])

  let postData = "";
  req.on('data', chunk => {
    postData += chunk;
  })
  req.on('end', () => {
    console.log(postData)
    let post = querystring.parse(postData);
    console.log(post)
    res.setHeader('Content-type', 'application/json')
    res.end(JSON.stringify(post))
  })

}).listen(8000)



/* const http=require('http');
const fs=require('fs');
let server=http.createServer((req,res)=>{
    fs.readFile(`www${req.url}`,(err,data)=>{
        console.log(`www${req.url}`);
        if(err){
            //返回状态
            res.writeHeader(404);
            //返回数据
            res.write('not found');
        }else{
            //write要么字符串要么二进制
            res.write(data);
        }
        res.end();
    })
});
server.listen(9999); */

/* GET数据
1. 在req.url里面
let {pathname,query} = url.parse(req.url,true)

POST数据
在body里面，比较大
let post = querystring.parse(str) */

/* const http=require('http');
const fs=require('fs');
const url=require('url');
const qs=require('querystring');
let server=http.createServer((req,res)=>{

    //GET数据
    let {pathname,query} = url.parse(req.url,true);
    let users={};
    let {user,pass} = query;

    //POST数据
    let str='';
    req.on('data',data=>{
        str+=data;
    });
    req.on('end',()=>{
        console.log(str)
        let post= qs.parse(str);
        switch(pathname){
            case '/reg':
                break;
            case '/login':
                break;
            default:
                fs.readFile('www${pathname}',(err,data)=>{
                    if(err){
                        res.writeHead(404);
                        res.write('not found');
                    }else{
                        res.write(data);
                    }
                })
            res.end();
        }
    })


});
server.listen(9999); */

//注册登录
/* const http=require('http');
const url=require('url');
const querystring=require('querystring');
const fs=require('fs');

let users={
//  'blue': '123456',
//  'zhangsan': '654321'
};

let server=http.createServer((req, res)=>{
  //GET
  let {pathname, query}=url.parse(req.url, true);

  //POST
  let str='';
  req.on('data', data=>{
    str+=data;
  });
  req.on('end', ()=>{
    let post=querystring.parse(str);

    let {user, pass}=query;

    //写东西
    switch(pathname){
      case '/reg':      //注册
        if(!user){
          res.write('{"err": 1, "msg": "user is required"}');
        }else if(!pass){
          res.write('{"err": 1, "msg": "pass is required"}');
        }else if(!/^\w{8,32}$/.test(user)){
          res.write('{"err": 1, "msg": "invaild username"}');
        }else if(/^['|"]$/.test(pass)){
          res.write('{"err": 1, "msg": "invaild password"}');
        }else if(users[user]){
          res.write('{"err": 1, "msg": "username already exsits"}');
        }else{
          users[user]=pass;
          res.write('{"err": 0, "msg": "success"}');
        }
        res.end();
        break;
      case '/login':    //登陆
        if(!user){
          res.write('{"err": 1, "msg": "user is required"}');
        }else if(!pass){
          res.write('{"err": 1, "msg": "pass is required"}');
        }else if(!/^\w{8,32}$/.test(user)){
          res.write('{"err": 1, "msg": "invaild username"}');
        }else if(/^['|"]$/.test(pass)){
          res.write('{"err": 1, "msg": "invaild password"}');
        }else if(!users[user]){
          res.write('{"err": 1, "msg": "no this user"}');
        }else if(users[user]!=pass){
          res.write('{"err": 1, "msg": "username or password is incorrect"}');
        }else{
          res.write('{"err": 0, "msg": "login success"}');
        }
        res.end();
        break;
      default:          //其他：文件
        fs.readFile(`www${pathname}`, (err, data)=>{
          if(err){
            res.writeHeader(404);
            res.write('Not Found');
          }else{
            res.write(data);
          }

          res.end();
        });
    }
  });
});
server.listen(9999); */

//文件上传服务器
/* const http=require('http');
const fs=require('fs');
const url=require('url');
const qs=require('querystring');
let server=http.createServer((req,res)=>{

    let arr=[]
    //POST数据
    let str='';
    req.on('data',data=>{
        arr.push(data);
    });
    req.on('end',()=>{
        let data = Buffer.concat(arr);
        console.log(data)
    })


});
server.listen(9999); */

//文件上传服务器
// const http=require('http');
// const common=require('./common');
// const fs=require('fs');
// const uuid=require('uuid/v4');

// let server=http.createServer((req, res)=>{
//   let arr=[];

//   req.on('data', data=>{
//     arr.push(data);
//   });
//   req.on('end', ()=>{
//     let data=Buffer.concat(arr);

//     //data
//     //解析二进制文件上传数据
//     let post={};
//     let files={};
//     if(req.headers['content-type']){
//       let str=req.headers['content-type'].split('; ')[1];
//       if(str){
//         let boundary='--'+str.split('=')[1];

//         //1.用"分隔符切分整个数据"
//         let arr=data.split(boundary);

//         //2.丢弃头尾两个数据
//         arr.shift();
//         arr.pop();

//         //3.丢弃掉每个数据头尾的"\r\n"
//         arr=arr.map(buffer=>buffer.slice(2,buffer.length-2));

//         //4.每个数据在第一个"\r\n\r\n"处切成两半
//         arr.forEach(buffer=>{
//           let n=buffer.indexOf('\r\n\r\n');

//           let disposition=buffer.slice(0, n);
//           let content=buffer.slice(n+4);

//           disposition=disposition.toString();

//           if(disposition.indexOf('\r\n')==-1){
//             //普通数据
//             //Content-Disposition: form-data; name="user"
//             content=content.toString();

//             let name=disposition.split('; ')[1].split('=')[1];
//             name=name.substring(1, name.length-1);

//             post[name]=content;
//           }else{
//             //文件数据
//             /*Content-Disposition: form-data; name="f1"; filename="a.txt"\r\n
//             Content-Type: text/plain*/
//             let [line1, line2]=disposition.split('\r\n');
//             let [,name,filename]=line1.split('; ');
//             let type=line2.split(': ')[1];

//             name=name.split('=')[1];
//             name=name.substring(1,name.length-1);

//             filename=filename.split('=')[1];
//             filename=filename.substring(1,filename.length-1);

//             let path=`upload/${uuid().replace(/\-/g, '')}`;

//             fs.writeFile(path, content, err=>{
//               if(err){
//                 console.log('文件写入失败', err);
//               }else{
//                 files[name]={filename, path, type};
//                 console.log(files);
//               }
//             });
//           }
//         });



//         //5.完成
//         console.log(post);
//       }
//     }

//     res.end();
//   });
// });
// server.listen(9999);

//流传文件 压缩
/* const http=require('http');
const fs=require('fs');
const zlib=require('zlib');

let server=http.createServer((req, res)=>{
  let rs=fs.createReadStream(`www${req.url}`);

  //rs.pipe(res);

  res.setHeader('content-encoding', 'gzip');

  let gz=zlib.createGzip();
  rs.pipe(gz).pipe(res);

  rs.on('error', err=>{
    res.writeHeader(404);
    res.write('Not Found');

    res.end();
  });
});
server.listen(9999); */