# pm2

npm install pm2 -g

pm2 --version

pm2 list

pm2 start

pm2 restart <appname/id>

pm2 stop <appname/id>

pm2 delete  <appname/id>

pm2 info <appname/id>

pm2 log <appname/id>

pm2 monit  <appname/id>

遇到报错会自动重启

pm2配置 

 pm2.conf.json

package.json     "prd": "cross-env NODE_ENV=production pm2 start pm2.conf.json"



```javascript
{
    "apps": {
        "name": "pm2-test-server",
        "script": "app.js",
        "watch": true,
        "ignore_watch": [
            "node_modules",
            "logs"
        ],
        "instances": 4,
        "error_file": "logs/err.log",
        "out_file": "logs/out.log",
        "log_date_format": "YYYY-MM-DD HH:mm:ss"
    }
}
```

