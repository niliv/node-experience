const {
  add,
  mul
} = require('./a')
const _ = require('lodash')

console.log(add(1, 2))
console.log(mul(2, 3))

console.log(_.concat([1, 2], 3))